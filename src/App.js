import React, {Component} from 'react';
import { BrowserRouter as Router, Route} from 'react-router-dom';
import Header from './components/layout/Header';
import Todos from './components/Todos';
import AddTodo from './components/AddTodo';
import About from './components/pages/About';
import './App.css';
import uuid from 'uuid';
import Axios from 'axios';

export default class App extends Component {
	// State that need to be accessed by multiple components
	state = {
		todos: [
			
		]
	}

	componentDidMount() {
		Axios.get('https://jsonplaceholder.typicode.com/todos?_limit=10')
			.then(res => this.setState({ todos: res.data }));
	}

	// Toggle Complete
	markComplete = (id) => {
		this.setState({ todos: this.state.todos.map(todo => {

				if(todo.id === id) {
					todo.completed = !todo.completed;
				}

				return todo;

			}) 
		})
		console.log('#markComplete from App.js', id);
	}

	// Add Todo
	addTodo = (title) => {
		Axios.post('https://jsonplaceholder.typicode.com/todos', {
			title,
			completed: false
		}).then( res => this.setState({ todos: [...this.state.todos, res.data] }))
		console.log('#addTodo from App.js', title);
	}

	// Delete Todo
	delTodo = (id) => {
		Axios.delete('https://jsonplaceholder.typicode.com/todos/${id}')
			.then(res => this.setState({ todos: [...this.state.todos.filter(todo => todo.id !== id)] }))
		console.log('#delTodo from App.js', id);
	}

	// Main Render
	render() {
		return (
			<Router>
				<div className="App">
					<div className="container">
						{/* Header Component */}
						<Header />

						<Route exact path="/" render={props => (

							<React.Fragment>
								{/* Add Todo Component */}
								<AddTodo addTodo={this.addTodo} />

								{/* Embedding A Component */}
								<Todos todos={this.state.todos} markComplete={this.markComplete} delTodo={this.delTodo} />
							</React.Fragment>

						)} />

						<Route path="/about" component={About} />

					</div>
				</div>
			</Router>
		);
	}
}
