import React from 'react'

export default function About() {
    return (
        <React.Fragment>
            <h1>About</h1>
            <p>This is the Todolist App v1.0.0, testing the router feature of ReactJS</p>
        </React.Fragment>
    )
}
