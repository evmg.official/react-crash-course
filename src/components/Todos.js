import React, { Component } from 'react';
import TodoItem from './TodoItem';
import PropTypes from 'prop-types';

export default class Todos extends Component {

    render() {
        
        {/* Mapping the props from App.JS, Mind the function that uses parenthesis instead of curly braces */}
        {/* Key is a unique value/id from the state */}

        return this.props.todos.map((todo) => (
            <TodoItem key={todo.id} todo={todo} markComplete={this.props.markComplete} delTodo={this.props.delTodo} />
        ));

    }

}

// PropTypes are sort of a validation for properties that a component should have
Todos.propTypes = {
    todos: PropTypes.array.isRequired,
    markComplete: PropTypes.func.isRequired,
    delTodo: PropTypes.func.isRequired
}