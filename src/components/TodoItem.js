import React, { Component } from 'react'
import PropTypes from 'prop-types';

export default class TodoItem extends Component {

    getStyle = () => {
        return {
            backgroundColor: '#f4f4f4',
            borderBottom: '1px dotted #ccc',
            padding: '10px',
            textDecoration: this.props.todo.completed ? 'line-through' : 'none' // Conditional CSS
        }
    }

    // to access 'this' on 'this.props' creating a function on arrow mode automatically binds it to parent class
    // markComplete = (e) => {
    //     console.log(this.props)
    // }

    render() {

        // Destructuring
        const { id, title, completed } = this.props.todo;

        return (
            <div style={this.getStyle()}>

                {/* Prop todo is passed from Todos Component */}
                <p>
                    <input type="checkbox" onChange={this.props.markComplete.bind(this, id)} checked={completed} />
                    {' '}
                    { title }
                    <button onClick={this.props.delTodo.bind(this, id)} style={btnStyle}>x</button>
                </p>

            </div>
        )
    }
}

// PropTypes are sort of a validation for properties that a component should have
TodoItem.propTypes = {
    todo: PropTypes.object.isRequired,
    markComplete: PropTypes.func.isRequired,
    delTodo: PropTypes.func.isRequired
}

const btnStyle = {
    backgroundColor: '#ff0000',
    color: '#fff',
    border: 'none',
    padding: '5px 9px',
    borderRadius: '100%',
    cursor: 'pointer',
    float: 'right'
}
